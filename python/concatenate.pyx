import numpy as np
cimport numpy as np
cimport cython

np.import_array()

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INTEGER

def concatenate(int n_features, 
    np.ndarray[INTEGER, ndim=1, mode='c'] X_indptr, 
    np.ndarray[INTEGER, ndim=1, mode='c'] X_indices, 
    np.ndarray[DOUBLE,  ndim=1, mode='c'] X_data, 
    np.ndarray[INTEGER, ndim=1, mode='c'] clusters,
    double ld):

   n_samples = X_indptr.shape[0] - 1

   X_data_ptr = <DOUBLE *>X_data.data
   X_indptr_ptr = <INTEGER *>X_indptr.data
   X_indices_ptr = <INTEGER *>X_indices.data
   clusters_ptr = <INTEGER *>clusters.data

   cdef np.ndarray[INTEGER, ndim=1, mode='c'] X_indptr_new = np.zeros(X_indptr.shape[0], dtype=np.int32)
   cdef np.ndarray[INTEGER, ndim=1, mode='c'] X_indices_new = np.zeros(X_indices.shape[0] * 2, dtype=np.int32)
   cdef np.ndarray[DOUBLE,  ndim=1, mode='c'] X_data_new = np.zeros(X_data.shape[0] * 2, dtype=np.float64)

   X_data_new_ptr = <DOUBLE *>X_data_new.data
   X_indptr_new_ptr = <INTEGER *>X_indptr_new.data
   X_indices_new_ptr = <INTEGER *>X_indices_new.data

   cdef int i = 0
   cdef int beg = 0
   cdef int end = 0
  
   for i in range(n_samples):
     beg = X_indptr_ptr[i] 
     end = X_indptr_ptr[i+1] 

     X_indptr_new_ptr[i] = beg_new = beg * 2
     if i == n_samples-1:
       X_indptr_new_ptr[i+1] = end_new = end * 2

     len_features = end - beg

     for j in range(len_features):
       X_indices_new_ptr[beg_new+j] = X_indices_ptr[beg+j]
       X_data_new_ptr[beg_new+j] = X_data_ptr[beg+j] * ld

       X_indices_new_ptr[beg_new+len_features+j] = X_indices_ptr[beg+j] + (clusters_ptr[i] + 1) * n_features 
       X_data_new_ptr[   beg_new+len_features+j] = X_data_ptr[beg+j]

   return (X_indptr_new, X_indices_new, X_data_new)

def concatenate_fuzzy(int n_features, 
    np.ndarray[INTEGER, ndim=1, mode='c'] X_indptr, 
    np.ndarray[INTEGER, ndim=1, mode='c'] X_indices, 
    np.ndarray[DOUBLE,  ndim=1, mode='c'] X_data, 
    np.ndarray[DOUBLE,  ndim=1, mode='c'] clusters_fuzzy, 
    int n_clusters, double ld):

   n_samples = X_indptr.shape[0] - 1

   X_data_ptr = <DOUBLE *>X_data.data
   X_indptr_ptr = <INTEGER *>X_indptr.data
   X_indices_ptr = <INTEGER *>X_indices.data
   clusters_ptr = <DOUBLE *>clusters_fuzzy.data

   cdef np.ndarray[INTEGER, ndim=1, mode='c'] X_indptr_new = np.zeros(X_indptr.shape[0], dtype=np.int32)
   cdef np.ndarray[INTEGER, ndim=1, mode='c'] X_indices_new = np.zeros(X_indices.shape[0] * (1+n_clusters), dtype=np.int32)
   cdef np.ndarray[DOUBLE,  ndim=1, mode='c'] X_data_new = np.zeros(X_data.shape[0] * (1+n_clusters), dtype=np.float64)

   X_data_new_ptr = <DOUBLE *>X_data_new.data
   X_indptr_new_ptr = <INTEGER *>X_indptr_new.data
   X_indices_new_ptr = <INTEGER *>X_indices_new.data

   cdef int i = 0
   cdef int j = 0
   cdef int k = 0

   cdef int beg = 0
   cdef int end = 0
   cdef int len_features = 0
  
   for i in range(n_samples):
     beg = X_indptr_ptr[i] 
     end = X_indptr_ptr[i+1] 

     X_indptr_new_ptr[i] = beg_new = beg * (1+n_clusters)
     if i == n_samples-1:
       X_indptr_new_ptr[i+1] = end_new = end * (1+n_clusters)

     len_features = end - beg

     for j in range(len_features):
       X_indices_new_ptr[beg_new+j] = X_indices_ptr[beg+j]
       X_data_new_ptr[beg_new+j] = X_data_ptr[beg+j] * ld

       for k in range(n_clusters):
         X_indices_new_ptr[beg_new+(k+1)*len_features+j] = X_indices_ptr[beg+j] + (k + 1) * n_features 
         X_data_new_ptr[   beg_new+(k+1)*len_features+j] = X_data_ptr[beg+j] * clusters_ptr[i*n_clusters+k]

   return (X_indptr_new, X_indices_new, X_data_new)
