import os, sys
import numpy as np
import scipy as sp

import scipy.sparse as ssp

from sklearn import cluster, utils, preprocessing
from sklearn import utils

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/python')
#sys.path.append('./utils')

from concatenate import concatenate, concatenate_fuzzy
from skmeans import SeqKMeans

class ClusteredTransformer:
  def __init__(self, est, n_clusters, ld=1.0, debug=False):
    self.est = est
    self.ld = ld #0.0 if ld == -1 else 1.0/np.sqrt(ld)

  def fit(self, X):
    self.n_features = X.shape[1]
    self.est.fit(X)
    self.n_clusters = len(self.est.centroids_)
    return self

  def fit_transform(self, X, y=None):
    self.n_features = X.shape[1]
    cluster_pred = self.est.fit_predict(X)
    self.n_clusters = len(self.est.centroids_)

    _X = X if isinstance(X, ssp.csr_matrix) else ssp.csr_matrix(X)
 
    return self._transform(_X, cluster_pred)

  def _transform(self, X, cluster_pred):
    cluster_pred = cluster_pred.reshape((cluster_pred.shape[0], 1))
    from sklearn.preprocessing import OneHotEncoder

    if self.ld != -1:
      _tmp = np.concatenate([np.zeros((cluster_pred.shape[0], 1)), \
          cluster_pred], axis=1)

      enc = OneHotEncoder(n_values=[1, self.n_clusters], dtype=np.float64)
      enc.fit(_tmp)
      X_new = enc.transform(_tmp)
    else:
      _tmp = cluster_pred

      enc = OneHotEncoder(n_values=[self.n_clusters], dtype=np.float64)
      enc.fit(_tmp)
      X_new = enc.transform(_tmp)

    if self.ld != -1:
      X_new = X_new.toarray()
      X_new[:,0] *= 1.0/np.sqrt(self.ld)
      
    return X_new

  def transform(self, X):
    cluster_pred = self.est.predict(X)

    _X = X if isinstance(X, ssp.csr_matrix) else ssp.csr_matrix(X)

    return self._transform(_X, cluster_pred)

class MiniBatchKMeansTransformer(ClusteredTransformer):
  def __init__(self, n_clusters = 10, ld=1.0):
    est = cluster.MiniBatchKMeans(n_clusters=n_clusters)
    ClusteredTransformer.__init__(self, est=est, n_clusters=n_clusters, ld=ld)

class KMeansTransformer(ClusteredTransformer):
  def __init__(self, n_clusters = 10, ld=1.0):
    est = cluster.KMeans(n_clusters=n_clusters)
    ClusteredTransformer.__init__(self, est=est, n_clusters=n_clusters, ld=ld)


class SeqKMeansTransformer(ClusteredTransformer):
  def __init__(self, n_clusters = 10, metric='euclid', ld=1.0):
    est = SeqKMeans(n_clusters=n_clusters, metric=metric)
    ClusteredTransformer.__init__(self, est=est, n_clusters=n_clusters, ld=ld)

def test(rate_pn=0.5):
  from sklearn import datasets
  from sklearn.svm import LinearSVC, SVC
  from sklearn.linear_model import PassiveAggressiveClassifier

  dataset = 'svmguide1'
  
  Xtr, ytr = datasets.load_svmlight_file('../data/%s' % dataset)
  Xte, yte = datasets.load_svmlight_file('../data/%s.t' % dataset)

  n_samples, n_features = Xtr.shape

  aver_s1 = []
  aver_s2 = []


  for shuffle in range(1, 11):
    Xtr_s, ytr_s = utils.shuffle(Xtr, ytr)

    clf = PassiveAggressiveClassifier(n_iter=1)
    clf.fit(Xtr_s, ytr_s)
    _score = clf.score(Xte, yte)
    aver_s1.append(_score)
    print _score, '->', 

    n_clusters = 100
    ld = 1000


    for tsf in [SeqKMeansTransformer(n_clusters=n_clusters, ld=ld),
          ]:

      tsf.fit(Xtr_s)
      Xtr_new = tsf.transform(Xtr_s)
      Xte_new = tsf.transform(Xte)

      clf = PassiveAggressiveClassifier(n_iter=1)
      clf.fit(Xtr_new, ytr_s)
      _score = clf.score(Xte_new, yte)
      aver_s2.append(_score)

      print _score

  print 'average',
  print np.mean(aver_s1), '->', np.mean(aver_s2)


if __name__ == '__main__':
  for j in range(1):
    for i in range(1):
      print i,
      test(i)
