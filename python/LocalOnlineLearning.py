import os, sys, logging
import numpy as np
import scipy as sp
import scipy.sparse as ssp

from joblib import Parallel, delayed

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/python')

from ClusteredTransformer import SeqKMeansTransformer
#from DoublePassiveAggressive import DoublePassiveAggressiveClassifier


class LocalOnlineLearningClassifier:
  def __init__(self, pa_type='PA1', C = 1.0, n_iter=5, \
      n_sampling=15, n_jobs=1, verbose=0, fit_intercept=True, \
      n_clusters=60, metric='euclid', ld=1.0):
    self.tsf = SeqKMeansTransformer(n_clusters, metric, ld)
    self.clf = DoublePassiveAggressiveClassifier(pa_type, C, n_iter, \
        n_sampling, n_jobs, verbose, fit_intercept)

  def partial_fit(self, X, y, classes):
    self.partial_fit_predict(X, y, classes)
    return self

  def partial_fit_predict(self, X, y, classes):
    X_local = self.tsf.fit_transform(X)
    return self.clf.partial_fit_predict(X, X_local, y, classes)

  def fit(self, X, y):
    classes = np.unique(y)
    return self.partial_fit(X, y, classes)

  def fit_predict(self, X, y):
    classes = np.unique(y)
    return self.partial_fit_predict(X, y, classes)

  def predict(self, X):
    X_local = self.tsf.transform(X)
    return self.clf.predict(X, X_local)

  def score(self, X, y):
    X_local = self.tsf.transform(X)
    return self.clf.score(X, X_local, y)

  def decision_function(self, X):
    X_local = self.tsf.transform(X)
    return self.clf.decision_function(X, X_local)
    

def parse_arguments():
  import argparse
  parser = argparse.ArgumentParser()

  #parser.add_argument('-m', '--mode', required=True', help='mode {train, test, train_test}')

  parser.add_argument('-tr', '--train_file',  required=True, help='file path of train file')
  parser.add_argument('-te', '--test_file',   required=True, help='file path of test file')
  parser.add_argument('-re', '--result_file', required=False, help='file path of output file')

  parser.add_argument('-t', '--type', default=1, type=int, choices=[1,2, 3], 
      help='clustering type: sequentiao k-means(1) \n mini-batch k-means(2) \n k-means(3)')

  parser.add_argument('-k', '--n_clusters', nargs='+', default=[60], type=int)
  parser.add_argument('-ld', '--Lambda', nargs='+', default=[1.0], type=float)

  #parser.add_argument('-m', '--metric', default='euclid')
  #parser.add_argument('-r', '--reduce', default=0, type=int)

  parser.add_argument('-C', '--C', nargs='+', default=[1.0], type=float)

  parser.add_argument('-it', '--n_iter', default=1, type=int)
  parser.add_argument('-ns', '--n_sampling', default=15, type=int)

  parser.add_argument('-j', '--n_jobs', default=10, type=int)
  parser.add_argument('-v', '--verbose', default=0, action='store_const', const=1)
  parser.add_argument('-bn', '--batch_num', default=10000, type=int)

  # to abandon
  parser.add_argument('-id', '--independent', default=False, action='store_const', const=True)

  args = parser.parse_args()

  return args


def main():
  from sklearn import datasets, utils

  args = parse_arguments()

  fn_train = args.train_file
  fn_test = args.test_file

  X, y = datasets.load_svmlight_file(fn_train)
  Xte, yte = datasets.load_svmlight_file(fn_test)

  X, y = utils.shuffle(X, y)
  
  clf = LocalOnlineLearningClassifier(n_clusters=args.n_clusters[0], \
      ld=args.Lambda[0],  C=args.C[0], n_iter=args.n_iter)

  clf.fit(X, y)
  print('{method} Result: \t{score}'.format(method=clf.__class__.__name__,  score=clf.score(Xte, yte)))

if __name__ == '__main__':
  main()

