import os, sys, logging
import numpy as np
import scipy as sp
import scipy.sparse as ssp

from sklearn.utils import compute_class_weight
from joblib import Parallel, delayed

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/python')

import wrapper_dpa

#logging.basicConfig(level=logging.INFO)

def fit_binary(W, intercept, fit_intercept, i, n_X1, X1_data, \
    X1_indptr, X1_indices, n_X2, X2_data, X2_indptr, X2_indices, \
    n_samples, y_i, pa_type, C, n_iter):

  val_pred, intercept = wrapper_dpa.wrapper_train(\
      W.reshape(n_X1*n_X2), intercept, fit_intercept, n_X1, n_X2, \
      X1_data, X1_indptr, X1_indices, \
      X2_data, X2_indptr, X2_indices, \
      y_i, n_samples, pa_type, C, n_iter)

  logging.debug('intercept:'+str(intercept))

  return i, W, intercept, val_pred

def decision_function(i, W, intercept, fit_intercept, X1, X2):
  n_samples, n_X1 = X1.shape
  n_X2 = X2.shape[1]

  val_pred = wrapper_dpa.wrapper_predict(W.reshape(n_X1*n_X2), \
      intercept, fit_intercept, n_X1, n_X2, X1.data, X1.indptr, \
      X1.indices, X2.data, X2.indptr, X2.indices, n_samples)
  
  return i, val_pred

class DoublePassiveAggressiveClassifier:
  def __init__(self, pa_type='PA1', C = 1.0, n_iter=1, \
      n_sampling=15, n_jobs=1, verbose=0, fit_intercept=True):

    self.C, self.pa_type = C, {'PA': 0, 'PA1': 1, 'PA2': 2}[pa_type]

    self.fit_intercept = 1 if fit_intercept == True else 0
    self.intercept = 0.0

    self.n_iter, self.n_sampling, self.n_jobs = n_iter, n_sampling, n_jobs
    self.verbose = verbose

    self.first_time_used = True

  def _label_map(self, y, i):
    y_i = np.ones(y.shape, dtype=np.float64, order="C")
    y_i[y != self.classes_[i]] = -1.0
    return y_i

  def fit(self, X1, X2, y):
    classes = np.unique(y)
    return self.partial_fit(X1, X2, y, classes)

  def fit_predict(self, X1, X2, y):
    classes = np.unique(y)
    return self.partial_fit_predict(X1, X2, y, classes)

  def partial_fit(self, X1, X2, y, classes):
    self.partial_fit_predict(X1, X2, y, classes)
    return self

  def partial_fit_predict(self, X1, X2, y, classes):
    if not isinstance(X1, ssp.csr_matrix):
      X1 = ssp.csr_matrix(X1)
    if not isinstance(X2, ssp.csr_matrix):
      X2 = ssp.csr_matrix(X2)

    n_samples, n_X1 = X1.shape
    n_X2 = X2.shape[1]

    self.interval = n_samples / self.n_sampling
    logging.debug("%d samples / %d sampling = %d interval" % (n_samples, self.n_sampling, self.interval)) 

    if self.first_time_used:
      self.classes_ = classes
      self.n_classes = classes.shape[0]

    if self.n_classes == 2:
      if self.first_time_used:
        self.W, self.intercept = [np.zeros((n_X1, n_X2))], [0]

      i, W, intercept, val_pred = fit_binary(\
          self.W[0], self.intercept[0], self.fit_intercept, 1, \
          X1.shape[1], X1.data, X1.indptr, X1.indices, \
          X2.shape[1], X2.data, X2.indptr, X2.indices, \
          X1.shape[0], self._label_map(y, 1), \
          self.pa_type, self.C, self.n_iter)

      self.W[0], self.intercept[0] = W, intercept
      indices = (val_pred > 0).astype(np.int)

    elif self.n_classes > 2:
      if self.first_time_used:
        self.W = [np.zeros((n_X1, n_X2)) for i in range(self.n_classes)]
        self.intercept = [0 for i in range(self.n_classes)]

      val_pred = [None for i in range(self.n_classes)]

      result = Parallel(n_jobs=self.n_jobs)(
          delayed(fit_binary)(\
              self.W[i], self.intercept[i], self.fit_intercept, i, \
              X1.shape[1], X1.data, X1.indptr, X1.indices, \
              X2.shape[1], X2.data, X2.indptr, X2.indices, \
              X1.shape[0], self._label_map(y, i), \
              self.pa_type, self.C, self.n_iter)
          for i in range(self.n_classes))

      for i, one_W, one_intercept, one_val_pred in result:
        self.W[i], self.intercept[i] = one_W, one_intercept
        val_pred[i] = one_val_pred.reshape(one_val_pred.shape[0], 1)
      
      val_pred = np.hstack(val_pred)
      indices = val_pred.argmax(axis=1)
    else:
      logging.error('n_classes < 2')


    self.first_time_used = False

    # --abundent--

    y_pred = self.classes_[indices]
    correct = (y_pred == y).astype(np.int)
    self.cumulate_errors = []
    intervals = n_samples / self.n_sampling
    end = intervals
    i = 0
    while (end <= n_samples):
      _tmp = float(np.sum(correct[0:end])) / end
      self.cumulate_errors.append(1-_tmp)
      i += 1
      end += intervals

    self.cumulate_errors = np.array(self.cumulate_errors)

    logging.info('self.cumulate_errors.shape:' + \
        str(self.cumulate_errors.shape))
    for ce in self.cumulate_errors:
      logging.info('Cum. Err. :%f' %  ce)

    # --abundent--

    return y_pred 
 
  def get_cumulate_errors(self):
    return self.cumulate_errors

  def predict(self, X1, X2):
    val_pred = self.decision_function(X1, X2)
    
    if len(val_pred.shape) == 1:
      indices = (val_pred > 0).astype(np.int)
    else:
      indices = val_pred.argmax(axis=1)

    return self.classes_[indices]
    
  def score(self, X1, X2, y):
    y_pred = self.predict(X1, X2)
    correct = (y_pred == y)
    return float(np.sum(correct)) / y.shape[0]

  def decision_function(self, X1, X2):
    if not isinstance(X1, ssp.csr_matrix):
      X1 = ssp.csr_matrix(X1)
    if not isinstance(X2, ssp.csr_matrix):
      X2 = ssp.csr_matrix(X2)

    n_samples, n_X1 = X1.shape
    n_X2 = X2.shape[1]

    ret = [None for W in self.W]

    result = Parallel(n_jobs=self.n_jobs)(
        delayed(decision_function)(i, W, self.intercept[i], self.fit_intercept, X1, X2)
        for i, W in enumerate(self.W))

    for i, one_val_pred in result:
      ret[i] = one_val_pred

    return ret[0] if len(ret) == 1 else np.hstack([r.reshape((r.shape[0], 1)) for r in ret])


def test():
  from sklearn import datasets
  from sklearn.linear_model import PassiveAggressiveClassifier as PAC
  from sklearn import utils
  dataset = 'svmguide1'
  fn_train = '../data/%s' % dataset
  fn_test = '../data/%s.t' % dataset

  X, y = datasets.load_svmlight_file(fn_train)
  print X.shape
  Xte, yte = datasets.load_svmlight_file(fn_test)
  print Xte.shape

  X, y = utils.shuffle(X, y)
  
  unit = ssp.csr_matrix(np.ones((X.shape[0], 1)))
  unit2 = ssp.csr_matrix(np.ones((Xte.shape[0], 1)))

  fit_intercept = True

  clf = DoublePassiveAggressiveClassifier(n_jobs=1, fit_intercept=fit_intercept)

  clf.partial_fit(X[0:1000,:], unit[0:1000,:], y[0:1000], np.unique(y))
  clf.partial_fit(X[1000:,:], unit[1000:,:], y[1000:], np.unique(y))
  print 'DoublePassiveAggressiveClassifier Result: %.5f' % clf.score(Xte, unit2, yte)

  clf = DoublePassiveAggressiveClassifier(n_jobs=10, fit_intercept=fit_intercept)
  clf.fit(unit, X, y)
  print 'DoublePassiveAggressiveClassifier Result: %.5f' % clf.score(unit2, Xte, yte)

  clf = PAC(n_iter=1, fit_intercept=fit_intercept, shuffle=False)
  clf.fit(X, y)
  print 'PassiveAggressiveClassifier Result: %.5f' % clf.score(Xte, yte)

if __name__ == '__main__':
  test()

