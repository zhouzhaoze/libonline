import numpy as np
cimport numpy as np

np.import_array()

cdef extern from "../src/CSR_Matrix.h":
  struct CSR_Matrix:
    double *data
    int *indptr
    int *indices
    int n_dimension
    int n_samples

cdef extern from "../src/dpa.h" nogil:
  struct problem:
    CSR_Matrix X1
    CSR_Matrix X2
    double *y
    int n_samples

  struct parameter:
    int pa_type
    double C
    int n_iter
    int interval
    double *cumulate_errors
    int *cumulate_errors_cnt

  struct model:
    double *W
    int n_dim1
    int n_dim2
    double bias
    double *intercept;
    int fit_intercept;

  void train(problem *prob, parameter *param, 
      model *mdl, double *pred_y)

  void predict(problem *prob, model *mdl, double *pred_y)

def wrapper_train(np.ndarray[double, ndim=1, mode="c"] W not None, 
    double intercept, int fit_intercept, 
    int n_X1, int n_X2,
    np.ndarray[double, ndim=1, mode="c"] X1_data not None, 
    np.ndarray[int, ndim=1, mode="c"]  X1_indptr not None, 
    np.ndarray[int, ndim=1, mode="c"] X1_indices not None, 
               
    np.ndarray[double, ndim=1, mode="c"] X2_data not None, 
    np.ndarray[int, ndim=1, mode="c"]  X2_indptr not None, 
    np.ndarray[int, ndim=1, mode="c"] X2_indices not None, 

    np.ndarray[double, ndim=1, mode="c"] y not None,
    int n_samples, int pa_type, double C, 
    int n_iter):

    #cdef np.ndarray cumulate_errors = np.zeros(n_samples / interval)
    #cdef np.ndarray cumulate_errors_cnt = np.zeros(interval, dtype=np.int32)
    cdef np.ndarray pred_y = np.zeros(n_samples)

    cdef CSR_Matrix X1
    X1.data = <double*> np.PyArray_DATA(X1_data)
    X1.indptr = <int*> np.PyArray_DATA(X1_indptr)
    X1.indices = <int*> np.PyArray_DATA(X1_indices)
    X1.n_samples = n_samples
    X1.n_dimension = n_X1

    cdef CSR_Matrix X2
    X2.data = <double*> np.PyArray_DATA(X2_data)
    X2.indptr = <int*> np.PyArray_DATA(X2_indptr)
    X2.indices = <int*> np.PyArray_DATA(X2_indices)
    X2.n_samples = n_samples
    X2.n_dimension = n_X2

    cdef problem prob
    prob.X1 = X1
    prob.X2 = X2
    prob.y = <double*> np.PyArray_DATA(y)
    prob.n_samples = n_samples

    cdef parameter param
    param.pa_type = pa_type
    param.C = C
    param.n_iter = n_iter

    cdef model mdl
    #double intercept
    mdl.W = <double*> np.PyArray_DATA(W)
    mdl.fit_intercept = fit_intercept
    mdl.n_dim1 = n_X1
    mdl.n_dim2 = n_X2
    mdl.intercept = &intercept;


    C_pred_y = <double*> np.PyArray_DATA(pred_y)
 
    with nogil:
        train(&prob, &param, &mdl, C_pred_y)

    return pred_y, intercept

def wrapper_predict(np.ndarray[double, ndim=1, mode="c"] W not None,
    double intercept, int fit_intercept, int n_X1, int n_X2, 

    np.ndarray[double, ndim=1, mode="c"] X1_data not None, 
    np.ndarray[int, ndim=1, mode="c"]  X1_indptr not None, 
    np.ndarray[int, ndim=1, mode="c"] X1_indices not None,
              
    np.ndarray[double, ndim=1, mode="c"] X2_data not None,
    np.ndarray[int, ndim=1, mode="c"]  X2_indptr not None,
    np.ndarray[int, ndim=1, mode="c"] X2_indices not None,

    int n_samples):

    cdef CSR_Matrix X1
    X1.data = <double*> np.PyArray_DATA(X1_data)
    X1.indptr = <int*> np.PyArray_DATA(X1_indptr)
    X1.indices = <int*> np.PyArray_DATA(X1_indices)
    X1.n_samples = n_samples
    X1.n_dimension = n_X1

    cdef CSR_Matrix X2
    X2.data = <double*> np.PyArray_DATA(X2_data)
    X2.indptr = <int*> np.PyArray_DATA(X2_indptr)
    X2.indices = <int*> np.PyArray_DATA(X2_indices)
    X2.n_samples = n_samples
    X2.n_dimension = n_X2

    cdef problem prob
    prob.X1 = X1
    prob.X2 = X2
    prob.n_samples = n_samples

    cdef model mdl
    mdl.W = <double*> np.PyArray_DATA(W)
    mdl.fit_intercept = fit_intercept
    mdl.n_dim1 = n_X1
    mdl.n_dim2 = n_X2
    mdl.intercept = &intercept

    cdef np.ndarray pred_y = np.zeros(n_samples)
    predict(&prob, &mdl, <double*> np.PyArray_DATA(pred_y))

    return pred_y
