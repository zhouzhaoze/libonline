from distutils.core import setup, Extension
import numpy
from Cython.Distutils import build_ext

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[
                 Extension('wrapper_skmeans', \
                    sources=['wrapper_skmeans.pyx', '../src/skmeans.c'],\
                    include_dirs=[numpy.get_include()]),

                 Extension('concatenate', \
                    sources=['concatenate.pyx'], \
                    include_dirs=[numpy.get_include()]),

                 Extension('wrapper_dpa', \
                    sources=['wrapper_dpa.pyx', '../src/dpa.c'],
                    include_dirs=[numpy.get_include()])

                 #Extension('wrapper_lpa',
                 #   sources=['wrapper_lpa.pyx', 'lpa.c'],
                 #   include_dirs=[numpy.get_include()])
                 ],
)
