#sequential-kmeans
import os, sys
import numpy as np
import scipy.sparse as ssp
import scipy.spatial.distance as spdist

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/python')

from wrapper_skmeans import wrapper_skmeans
from wrapper_skmeans import wrapper_skmeans_transform
from wrapper_skmeans import wrapper_skmeans_predict

class SeqKMeans:
  def __init__(self, n_clusters=3, metric='euclid', mask_feature=None, debug=True):
    self.debug = debug
    self.n_clusters = n_clusters
    #self.labels_ = range(1, self.n_clusters+1)
    self.metric = {'euclid': 0, 'cosine': 1, 'inner_product': 2}[metric]
    self.first_time_used = True
    if mask_feature is not None:
      self.mask_feature = np.array(mask_feature, dtype=np.int32)
    else:
      self.mask_feature = mask_feature

  def fit_predict(self, X):
    if not isinstance(X, ssp.csr_matrix):
      X = ssp.csr_matrix(X)

    n_samples, n_features = X.shape

    if self.mask_feature is None:
      self.mask_feature = np.ones(n_features, dtype=np.int32)

    if self.first_time_used:
      self.centroids_ = np.zeros((self.n_clusters, n_features))
      self.weight_centroids = np.zeros(self.n_clusters, dtype=np.int32)

    cluster_pred = np.zeros(n_samples, dtype=np.int32)

    wrapper_skmeans(self.centroids_.reshape(self.n_clusters * n_features), \
        self.weight_centroids, self.n_clusters, n_features, X.data, X.indptr, \
        X.indices, n_samples, cluster_pred, self.mask_feature, self.metric, \
        1 if self.first_time_used else 0)

    self.first_time_used = False

    return cluster_pred

  def fit(self, X):
    self.fit_predict(X)
    return self

  def predict(self, X):
    if not isinstance(X, ssp.csr_matrix):
      X = ssp.csr_matrix(X)

    n_samples, n_features = X.shape
    n_clusters = self.n_clusters
 
    cluster_pred = np.zeros(n_samples, dtype=np.int32)

    wrapper_skmeans_predict(self.centroids_.reshape(n_clusters * n_features), \
        self.n_clusters, n_features, X.data, X.indptr, X.indices, n_samples, \
        cluster_pred, self.mask_feature, self.metric)

    return cluster_pred

  def transform(self, X, distance_type='euclidean'):
    n_samples, n_features = X.shape
    n_clusters = self.n_clusters
    dist_matrix = np.zeros((n_samples, n_clusters))
    
    if ssp.issparse(X):
      X = X.tocsr()
    else:
      X = ssp.csr_matrix(X)

    wrapper_skmeans_transform(self.centroids_.reshape(n_clusters * n_features), \
        self.n_clusters, n_features, X.data, X.indptr, X.indices, n_samples, \
        dist_matrix.reshape(n_samples * n_clusters), self.mask_feature, self.metric)

    return dist_matrix

def test_new_old_method():
  from sklearn import datasets
  from sklearn import utils
  iris = datasets.load_iris()

#  X, y = ssp.csr_matrix(iris['data']), iris['target']
#  Xte, yte = X, y
  X, y = datasets.load_svmlight_file('data/svmguide1')
  Xte, yte = datasets.load_svmlight_file('data/svmguide1.t')

  #X, y = utils.shuffle(X, y)
  mask_feature = np.ones(4)
  _t = mask_feature
  _tt = []

  X1 = X
  #Xte1 = Xte

  #tsf = SeqKMeans(n_clusters=4, metric='inner_product', debug=False)#, mask_feature=[1,1,1,1])
  #a1 = tsf.fit1(X1)
  #b1 = tsf.predict1(X1)
  #c1 = tsf.transform1(X1)
  #pred1 = tsf.predict1(Xte1)
  #print tsf.centroids_


  tsf = SeqKMeans(n_clusters=4, debug=True, mask_feature=_t)
  #a = tsf.fit(X)
  a = tsf.fit(X[0:1000])
  a = tsf.fit(X[1000:])
  b = tsf.predict(X)
  #return
  c = tsf.transform(X)
  pred = tsf.predict(Xte)
  print tsf.centroids_


#  print sum(b1 == b), '/', b.shape
  #print sum(pred1 == pred), '/', pred.shape

if __name__ == '__main__':

  test_new_old_method()
