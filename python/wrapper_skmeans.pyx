import numpy as np
cimport numpy as np

np.import_array()

cdef extern from "../src/CSR_Matrix.h":
  struct CSR_Matrix:
    double *data
    int *indptr
    int *indices
    int n_dimension
    int n_samples

cdef extern from "../src/skmeans.h":
  struct model:
    int first_time_used
    double *centroids
    int *weight_centroids
    int n_centroid
    int n_feature

  void skmeans_clustering(model *mdl, CSR_Matrix *X, 
      int *cluster_pred, int *mask_feature, int metric)

  void skmeans_dist(model *mdl, CSR_Matrix *X, 
      double *dist_matrix, int *mask_feature, int metric)

  void skmeans_predict(model *mdl, CSR_Matrix *X, 
      int *cluster_pred, int *mask_feature, int metric)

def wrapper_skmeans(
    np.ndarray[double, ndim=1, mode="c"] centroids not None,
    np.ndarray[int, ndim=1, mode="c"] weight_centroids not None, 
    int n_centroids, int n_features, 
    np.ndarray[double, ndim=1, mode="c"] X_data not None, 
    np.ndarray[int, ndim=1, mode="c"] X_indptr not None, 
    np.ndarray[int, ndim=1, mode="c"] X_indices not None, 
    int n_samples, 
    np.ndarray[int, ndim=1, mode="c"] cluster_pred not None, 
    np.ndarray[int, ndim=1, mode="c"] mask_feature not None, 
    int metric, int first_time_used):

  cdef CSR_Matrix X
  X.data        = <double*> np.PyArray_DATA(X_data)
  X.indptr      = <int*> np.PyArray_DATA(X_indptr)
  X.indices     = <int*> np.PyArray_DATA(X_indices)
  X.n_dimension = n_features
  X.n_samples   = n_samples

  cdef model mdl
  mdl.first_time_used = first_time_used
  mdl.centroids = <double*> np.PyArray_DATA(centroids)
  mdl.weight_centroids = <int*> np.PyArray_DATA(weight_centroids)
  mdl.n_centroid = n_centroids
  mdl.n_feature = n_features

  skmeans_clustering(&mdl, &X, <int*> np.PyArray_DATA(cluster_pred),
      <int*> np.PyArray_DATA(mask_feature), metric)

def wrapper_skmeans_transform(
    np.ndarray[double, ndim=1, mode="c"] centroids not None, 
    int n_centroids, int n_features, 
    np.ndarray[double, ndim=1, mode="c"] X_data not None, 
    np.ndarray[int, ndim=1, mode="c"] X_indptr not None, 
    np.ndarray[int, ndim=1, mode="c"] X_indices not None, 
    int n_samples, 
    np.ndarray[double, ndim=1, mode="c"] dist_matrix not None, 
    np.ndarray[int, ndim=1, mode="c"] mask_feature not None, 
    int metric):

  cdef CSR_Matrix X
  X.data        = <double*> np.PyArray_DATA(X_data)
  X.indptr      = <int*> np.PyArray_DATA(X_indptr)
  X.indices     = <int*> np.PyArray_DATA(X_indices)
  X.n_dimension = n_features
  X.n_samples   = n_samples

  cdef model mdl
  mdl.centroids = <double*> np.PyArray_DATA(centroids)
  mdl.n_centroid = n_centroids
  mdl.n_feature = n_features

  skmeans_dist(&mdl, &X, <double*> np.PyArray_DATA(dist_matrix),
              <int*> np.PyArray_DATA(mask_feature), metric)

def wrapper_skmeans_predict(
    np.ndarray[double, ndim=1, mode="c"] centroids not None, 
    int n_centroids, int n_features, 
    np.ndarray[double, ndim=1, mode="c"] X_data not None, 
    np.ndarray[int, ndim=1, mode="c"] X_indptr not None,
    np.ndarray[int, ndim=1, mode="c"] X_indices not None, 
    int n_samples, 
    np.ndarray[int, ndim=1, mode="c"] cluster_pred not None, 
    np.ndarray[int, ndim=1, mode="c"] mask_feature not None, 
    int metric):

  cdef CSR_Matrix X
  X.data        = <double*> np.PyArray_DATA(X_data)
  X.indptr      = <int*> np.PyArray_DATA(X_indptr)
  X.indices     = <int*> np.PyArray_DATA(X_indices)
  X.n_dimension = n_features
  X.n_samples   = n_samples

  cdef model mdl
  mdl.centroids = <double*> np.PyArray_DATA(centroids)
  mdl.n_centroid = n_centroids
  mdl.n_feature = n_features


  skmeans_predict(&mdl, &X, <int*> np.PyArray_DATA(cluster_pred),
      <int*> np.PyArray_DATA(mask_feature), metric)

