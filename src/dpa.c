#include "dpa.h"

#define SINGLE_THREADED
#define NUM_THREADS 24

inline double min(double a, double b) { return (a < b) ? a : b; }

double norm_fro(struct CSR_Matrix *x1, struct CSR_Matrix *x2) {
  double *x1_data = x1->data + x1->indptr[0];
  int x1_nnz = x1->indptr[1] - x1->indptr[0];

  double *x2_data = x2->data + x2->indptr[0];
  int x2_nnz = x2->indptr[1] - x2->indptr[0];

  double norm = 0.0;
  int i, j;
  for (i = 0; i < x1_nnz; ++i) {
    for (j = 0; j < x2_nnz; ++j) {
      double _tmp = x1_data[i] * x2_data[j];
      norm += _tmp * _tmp;
    }
  }
  return norm;
}

double x1T_W_x2(struct model *p_mdl, struct CSR_Matrix *x1,
        struct CSR_Matrix *x2) {
  double *x1_data = x1->data + x1->indptr[0];
  int *x1_indices = x1->indices + x1->indptr[0];
  int x1_nnz = x1->indptr[1] - x1->indptr[0];

  double *x2_data = x2->data + x2->indptr[0];
  int *x2_indices = x2->indices + x2->indptr[0];
  int x2_nnz = x2->indptr[1] - x2->indptr[0];
  int x2_n_featrues = x2->n_dimension;

  double product = 0.0;
  int i, j;
  double *W = p_mdl->W;
  for (i = 0; i < x1_nnz; ++i) {
    for (j = 0; j < x2_nnz; ++j) {
      product += W[x1_indices[i] * x2_n_featrues + x2_indices[j]] *
        x1_data[i] * x2_data[j];
    }
  }

  if (p_mdl->fit_intercept == 1) 
    product += *p_mdl->intercept;

  return product;
}

void update_model(struct model *p_model, struct CSR_Matrix *x1, 
        struct CSR_Matrix *x2, double constant) {

  double *x1_data = x1->data + x1->indptr[0];
  int *x1_indices = x1->indices + x1->indptr[0];
  int x1_nnz = x1->indptr[1] - x1->indptr[0];

  double *x2_data = x2->data + x2->indptr[0];
  int *x2_indices = x2->indices + x2->indptr[0];
  int x2_nnz = x2->indptr[1] - x2->indptr[0];
  int x2_n_featrues = x2->n_dimension;

  double *W = p_model->W;

  int i, j;
  for (i = 0; i < x1_nnz; ++i) {
    for (j = 0; j < x2_nnz; ++j) {
      W[x1_indices[i] * x2_n_featrues + x2_indices[j]] += 
             x1_data[i] * x2_data[j] * constant;
    }
  }
  if (p_model->fit_intercept/* == 1*/) {
    *p_model->intercept += constant;
  }
}

double loss(double p, double y) {
  double z = p * y;
  if (z <= 1.0)
    return 1.0 - z;
  else
    return 0.0;
}

double sign(double val) { return val > 0.0? 1.0: -1.0; }

#ifdef SINGLE_THREADED
void train(struct problem* prob, struct parameter *param,
        struct model* mdl, double *pred_y) {
#else 

struct thread_data {
  int thread_id;
  struct model *mdl;
  struct parameter *param;
  struct problem *prob;
  double *pred_y;
};
 
void *train(void *thread_arg) {
  //PyThreadState *_state;
  //_state = PyEval_SaveThread();
  //Py_BEGIN_ALLOW_THREADS
  //PyGILState_STATE gstate;
  //gstate = PyGILState_Ensure();
  struct thread_data *td = (struct thread_data *) thread_arg;
  int thread_id = td->thread_id;
  struct model* mdl = td->mdl;
  struct parameter *param = td->param;
  struct problem *prob = td->prob;
  double *pred_y = td->pred_y;

#endif

  struct CSR_Matrix x1;  x1.data = prob->X1.data;  x1.indices = prob->X1.indices;  x1.n_dimension = prob->X1.n_dimension;  x1.n_samples = 1;
  struct CSR_Matrix x2;  x2.data = prob->X2.data;  x2.indices = prob->X2.indices;  x2.n_dimension = prob->X2.n_dimension;  x1.n_samples = 1;
 
  int i = 0;
  //test();
  //int interval = prob->n_samples / param->n_sampling;
  int error_cnt = 0;

  int n_epoch = 0;
  for (n_epoch = 0; n_epoch < param->n_iter; ++n_epoch) {

  for (i = 0; i < prob->n_samples; ++i) {
#ifdef SINGLE_THREADED
#else
    if (i % NUM_THREADS != thread_id) {
      continue;
    } else {
      //printf("thread_id:%d\n", thread_id);
    }
#endif
    x1.indptr = prob->X1.indptr + i;
    x2.indptr = prob->X2.indptr + i;

    //double p = x1T_W_x2(mdl, &prob->X1, &prob->X2, i);
    double p = x1T_W_x2(mdl, &x1, &x2);

    pred_y[i] = p;

    double y_hat = sign(p);

    if (y_hat * prob->y[i] < 0) {
      error_cnt += 1;
    }

    /*
    if ((i+1) % param->interval == 0) {
      param->cumulate_errors_cnt[(i+1) / param->interval - 1] = error_cnt;
      param->cumulate_errors[(i+1) / param->interval - 1] = error_cnt / (double) (i+1);
    } */

    double update = norm_fro(&x1, &x2);

    if (update == 0)
        continue;

    if (param->pa_type == PA) {
      update = loss(p, prob->y[i]) / update;
    } else if (param->pa_type == PA1) {
      update = min(param->C, loss(p, prob->y[i]) / update);
    } else if (param->pa_type == PA2) {
      update = loss(p, prob->y[i]) / (update + 0.5 / param->C);
    } else {
      //return ;
    }

    update *= prob->y[i];

    if (update != 0.0) {
      update_model(mdl, &x1, &x2, update);
    }
  }

  }
  //PyGILState_Release(gstate);
  //Py_END_ALLOW_THREADS
  //PyEval_RestoreThread(_state);
  //return NULL;
}

void predict(struct problem *prob, struct model *mdl, double *pred_y) {
  int i = 0;

  struct CSR_Matrix x1;  x1.data = prob->X1.data;  x1.indices = prob->X1.indices;  x1.n_dimension = prob->X1.n_dimension;  x1.n_samples = 1;
  struct CSR_Matrix x2;  x2.data = prob->X2.data;  x2.indices = prob->X2.indices;  x2.n_dimension = prob->X2.n_dimension;  x1.n_samples = 1;

  for (i = 0; i < prob->n_samples; ++i) {
    x1.indptr = prob->X1.indptr + i;
    x2.indptr = prob->X2.indptr + i;

    double p = x1T_W_x2(mdl, &x1, &x2);

    pred_y[i] = p;
  }
}

