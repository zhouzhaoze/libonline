#include <stdio.h>
#include <math.h>

#include "skmeans.h"

inline double eqlidean_distance_sqr_sparse2(double *centroid, 
    struct CSR_Matrix *x, int *mask_feature) {

  double *x_data = x->data + x->indptr[0];
  int *x_indices = x->indices + x->indptr[0];
  int nnz = x->indptr[1] - x->indptr[0];
  int n_features = x->n_dimension;

  double sum = 0.0;
  int i = 0;
  double _tmp = 0.0;
  for (i = 0; i < n_features; ++i) {
    if (mask_feature[i] == 1) 
      sum += centroid[i] * centroid[i];
  }
  for (i = 0; i < nnz; ++i) {
    if (mask_feature[x_indices[i]]/* == 1*/) {
      _tmp = centroid[x_indices[i]];
      sum -= (_tmp * _tmp);
      _tmp = centroid[x_indices[i]] - x_data[i];
      sum += (_tmp * _tmp);
    }
  }
  return sum;
}

double inner_product_sparse(double *centroid, 
    struct CSR_Matrix *x, int *mask_feature) { 

  double *x_data = x->data + x->indptr[0];
  int *x_indices = x->indices + x->indptr[0];
  int nnz = x->indptr[1] - x->indptr[0];
  int n_features = x->n_dimension;

  double inner_product = 0.0;
  double norm_x = 0.0;
  int i = 0;
  for (i = 0; i < nnz; ++i) {
    if (mask_feature[x_indices[i]] /*!= 0*/) {
      norm_x += centroid[x_indices[i]] * centroid[x_indices[i]];
      inner_product += centroid[x_indices[i]] * x_data[i];
    }
  }
  norm_x = sqrt(norm_x);
  return inner_product/norm_x;
}

double cosine_distance_sqr_sparse(double *centroid, 
    struct CSR_Matrix *x, int *mask_feature) { 

  double *x_data = x->data + x->indptr[0];
  int *x_indices = x->indices + x->indptr[0];
  int nnz = x->indptr[1] - x->indptr[0];
  int n_features = x->n_dimension;

  double inner_product = 0.0;
  double norm_centroid = 0.0;
  double norm_x = 0.0;
  int i = 0;
  for (i = 0; i < n_features; ++i) {
    if (mask_feature[i]/*!= 0*/)
      norm_centroid += centroid[i] * centroid[i];
  }
  norm_centroid = sqrt(norm_centroid);
  for (i = 0; i < nnz; ++i) {
    int _t = x_indices[i];
    if (mask_feature[_t] /*!= 0*/) {
      norm_x += centroid[_t] * centroid[_t];
      inner_product += centroid[_t] * x_data[i];
    }
  }
  norm_x = sqrt(norm_x);
  
  return inner_product/(norm_x * norm_centroid);
}

double eqlidean_distance_sqr(double *centroid, 
    int n_features, double *x_data) {
  double sum = 0.0;
  int i = 0;
  double _tmp = 0.0;
  for (i = 0; i < n_features; ++i) {
    _tmp = centroid[i] - x_data[i];
    sum += _tmp * _tmp;
  }
  return sum;
}

void getDistanceSparse2(struct model *mdl, struct CSR_Matrix *x, 
    double *distances, int *mask_feature, int metric) {

  double *centroids = mdl->centroids;
  int n_centroids = mdl->n_centroid;
  int n_features = mdl->n_feature;

  int i = 0;
  double *centroid;
  double _distance = 0;
  for (i = 0; i < n_centroids; ++i) {
    centroid = centroids + n_features * i;

    if (metric == INNERPRODUCT) {
      _distance = inner_product_sparse(
          centroid, 
          x, mask_feature);
    } else if (metric == COSINE) {
      _distance = cosine_distance_sqr_sparse(
          centroid, x,
          mask_feature);
    } else {
      _distance = eqlidean_distance_sqr_sparse2(
          centroid, 
          x, mask_feature);
    }

    distances[i] = _distance;
  }
}

int getNearestIndex(struct model *mdl, struct CSR_Matrix *x, 
    int *mask_feature, int metric) {

  double *centroids = mdl->centroids;
  int n_centroids = mdl->n_centroid;
  int n_features = mdl->n_feature;

  int i = 0;
  double *centroid;
  double _distance = 0;
  int index_nearest_centroid = 0;
  double min_distance = -1;

  for (i = 0; i < n_centroids; ++i) {
    centroid = centroids + n_features * i;

    if (metric == INNERPRODUCT) {
      _distance = inner_product_sparse(
          centroid, x, mask_feature);
    } else if (metric == COSINE) {
      ;
    }
    else {
      _distance = eqlidean_distance_sqr_sparse2(
          centroid, x, mask_feature);
    } 
    if (i == 0) {
      min_distance = _distance;
      index_nearest_centroid = 0;
    }
    else if (_distance < min_distance) {
      min_distance = _distance;
      index_nearest_centroid = i;
    }
  }
  return index_nearest_centroid;
}

inline void update(double *centroid, double p, 
    struct CSR_Matrix *x, int *mask_features) {
  double *x_data = x->data + x->indptr[0];
  int *x_indices = x->indices + x->indptr[0];
  int nnz = x->indptr[1] - x->indptr[0];
  int n_features = x->n_dimension;
 
  int i = 0;
  for (i = 0; i < n_features; ++i) {
    if (mask_features[i] == 1) {
      centroid[i] *= 1.0 - p;
    }
  }
  for (i = 0; i < nnz; ++i) {
    if (mask_features[x_indices[i]] == 1) {
      centroid[x_indices[i]] += p * x_data[i];
    }
  }
}

void skmeans_clustering(struct model *mdl, struct CSR_Matrix *X, 
         int *pred_centroid, int *mask_feature, int metric) {
  int i = 0;
  double *centroid = NULL;
  int index_nearest_centroid;
  double _p = 0.0;
  struct CSR_Matrix _x; _x.n_dimension = X->n_dimension;  _x.data = X->data;  _x.indices = X->indices; 

  for (i = 0; i < X->n_samples; ++i) {
    //printf("processing %d\n", i);
    _x.indptr = X->indptr + i;
    if (mdl->first_time_used == 1 && i < mdl->n_centroid) {
      index_nearest_centroid = i;
      _p = 1.0;
    } else {
      index_nearest_centroid = getNearestIndex(mdl, &_x, mask_feature, metric);
      _p = 1.0 / mdl->weight_centroids[index_nearest_centroid];
    }

    pred_centroid[i] = index_nearest_centroid;
    centroid = mdl->centroids + mdl->n_feature * index_nearest_centroid;

    update(centroid, _p, &_x, mask_feature);

    mdl->weight_centroids[index_nearest_centroid] += 1;
  }
}

void skmeans_dist(struct model *mdl, struct CSR_Matrix *X,
    double *dist_matrix, int *mask_feature, int metric) {
  int i = 0;
  double *distances = NULL;
  struct CSR_Matrix _x; 
  _x.n_dimension = X->n_dimension; 
  _x.data = X->data; _x.indices = X->indices; 

  for (i = 0; i < X->n_samples; ++i) {
    _x.indptr = X->indptr + i;
    distances = dist_matrix + i * mdl->n_centroid;
    getDistanceSparse2(mdl, &_x, distances, mask_feature, metric);
  }
}

void skmeans_predict(struct model *mdl, struct CSR_Matrix *X,
    int *cluster_pred, int *mask_feature, int metric) {
  int i = 0;
  struct CSR_Matrix _x; 
  _x.n_dimension = X->n_dimension; _x.data = X->data; _x.indices = X->indices; 

  for (i = 0; i < X->n_samples; ++i) {
    _x.indptr = X->indptr + i;
    int min_index = getNearestIndex(mdl, &_x, mask_feature, metric);
    cluster_pred[i] = min_index;
  }
}
