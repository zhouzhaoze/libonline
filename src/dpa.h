#include <stdio.h>
#include <pthread.h>

#include "CSR_Matrix.h"

#define PA 0
#define PA1 1
#define PA2 2

struct problem {
  struct CSR_Matrix X1;
  struct CSR_Matrix X2;
  double *y;
  int n_samples;
};

struct parameter {
  int pa_type;
  double C;
  int n_iter;
  int interval;
  double *cumulate_errors;
  int *cumulate_errors_cnt;
};

struct model {
  double *W;
  int n_dim1;
  int n_dim2;
  //double bias;
  double *intercept;
  int fit_intercept;
};

void train(struct problem *prob, struct parameter *param,
    struct model *mdl, double *pred_y);

void predict(struct problem *prob, struct model* mdl, 
    double *pred_y);

