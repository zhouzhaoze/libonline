#ifndef CSR_MATRIX_INCLUDED
#define CSR_MATRIX_INCLUDED
struct CSR_Matrix {
    double *data;
    int *indptr;
    int *indices;
    int n_dimension;
    int n_samples;
};
#endif

