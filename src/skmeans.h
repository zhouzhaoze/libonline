#include "CSR_Matrix.h"

#define EUCLUDEAN 0
#define COSINE 1
#define INNERPRODUCT 2

struct model {
  int first_time_used;
  double *centroids;
  int *weight_centroids;
  int n_centroid;
  int n_feature;
};

void skmeans_clustering(struct model *mdl, struct CSR_Matrix *X, 
    int *pred_cluster, int *mask_feature, int metric);

void skmeans_dist(struct model *mdl, struct CSR_Matrix *X,
    double *dist_matrix, int *mask_feature, int metric);

void skmeans_predict(struct model *mdl, struct CSR_Matrix *X,
    int *cluster_pred, int *mask_feature, int metric);


